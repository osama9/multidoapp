import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EnvReducer from './EnvReducer';
import TodoReducer from './TodoReducer';
import DateReducer from './DateReducer';
import NotificationReducer from './NotificationReducer';
import TaskReducer from './TaskReducer';
import ModalReducer from './ModalReducer';
import CtxmenuReducer from './CtxmenuReducer';

export default combineReducers({
  auth: AuthReducer,
  env: EnvReducer,
  todo: TodoReducer,
  date: DateReducer,
  notification: NotificationReducer,
  task: TaskReducer,
  modal: ModalReducer,
  ctxmenu: CtxmenuReducer
});
