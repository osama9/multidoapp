import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  logoutVisible: false,
  settingsVisible: false,
  howtoVisible: false,
  howtoIsFirst: false,
  menuVisible: false,
  taskformVisible: false,
  taskformType: 'add',  // 'add' or 'edit'
  curtainVisible: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.MODAL.LOGOUTSET:
        return { ...state, logoutVisible: action.payload };
      case ACTYPS.MODAL.SETTINGSSET:
        return { ...state, settingsVisible: action.payload };
      case ACTYPS.MODAL.HOWTOSET:
        return { ...state, howtoVisible: action.payload };
      case ACTYPS.MODAL.HOWTOTYPE:
        return { ...state, howtoIsFirst: action.payload };
      case ACTYPS.MODAL.MENUSET:
        return { ...state, menuVisible: action.payload };
      case ACTYPS.MODAL.TASKFORMSET:
        return { ...state, taskformVisible: action.payload };
      case ACTYPS.MODAL.TASKFORMTYPESET:
        return { ...state, taskformType: action.payload };
      case ACTYPS.MODAL.CURTAINSET:
        return { ...state, curtainVisible: action.payload };
      default:
        return state;
    }
};
