import React, { Component } from 'react';
import { Text, View, Animated, Easing } from 'react-native';
import { connect } from 'react-redux';
import { modalLogoutSet, todoReset, authReset, dateReset } from '../../actions';
import Modaler from './Modaler';
import { Button, TextButton } from '../common';
import { Def, Txt } from '../../config';
import { Tracker, Gac } from '../../ga';

class Logout extends Component {

  state = {
    appeard: false,
    opacity: new Animated.Value(0),
    bottom: new Animated.Value(2000),
    animDuration: Def.modalDuration,
    animBounce: 0.8
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.auth.loggedin !== nextProps.auth.loggedin) return true;
    if (this.props.visible !== nextProps.visible) return true;
    return false;
  }

  componentDidUpdate() {
    if (this.props.auth.loggedin && this.props.visible) {
      Tracker.screenView(Gac.scr.logout);
      Animated.sequence([
        Animated.delay(10),
        Animated.timing(this.state.bottom, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration })
      ]).start();
    }

    if (!this.props.auth.loggedin && this.props.visible) {
      Animated.timing(this.state.bottom, { toValue: 2000, duration: this.state.animDuration }).start(() => {
        this.props.authReset();
        this.props.dateReset();
        this.props.modalLogoutSet(false);
      });
    }
  }


  onCancelPress() {
    Animated.timing(this.state.bottom, { toValue: 2000, duration: this.state.animDuration }).start(() => {
      this.props.modalLogoutSet(false);
    });
    Tracker.userAction(Gac.usract.formlogoutcancel);
  }

  onLogoutPress() {
    this.props.todoReset();
    this.props.dateReset();
    Tracker.userAction(Gac.usract.formlogout);
  }

  renderForm() {
    return (
      <View style={styles.container}>
        <View style={[styles.section, styles.head]}>
          <Text style={styles.headText}>{Txt.logout.title.Break()}</Text>
        </View>

        <View style={styles.section}>
          <Button onPress={this.onLogoutPress.bind(this)}>{Txt.logout.button}</Button>
        </View>

        <View style={[styles.section, styles.foot]}>
          <TextButton onPress={this.onCancelPress.bind(this)}>{Txt.logout.cancel}</TextButton>
        </View>
      </View>
    );
  }

  renderWrapperStyle() {
    const bottom = this.state.bottom;
    return [styles.wrapper, { bottom }];
  }

  renderWrapper() {
    if (this.props.env.wide) {
      return (
        <Animated.View style={this.renderWrapperStyle()}>
          <View style={styles.contentcont}>
            <View style={styles.box}>
              {this.renderForm()}
            </View>
          </View>
        </Animated.View>
      );
    }

    return (
      <Animated.View style={this.renderWrapperStyle()}>
        <View style={styles.contentcont}>
          {this.renderForm()}
        </View>
      </Animated.View>
    );
  }

  render() {
    return (
      <Modaler {...this.props} onRequestClose={this.onCancelPress.bind(this)}>
        <View style={styles.inmodaler}>
          {this.renderWrapper()}
        </View>
      </Modaler>
    );
  }
}


const styles = {
  inmodaler: {
    flex: 1, // is important
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    position: 'absolute',
    zIndex: 0,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    overflow: 'hidden',
    backgroundColor: Def.colorBlue
  },
  contentcont: {
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    alignSelf: 'center',
    width: 400,
    height: 350,
  },
  container: {
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  section: {
    flexDirection: 'row',
    paddingBottom: 10
  },
  head: {
    paddingBottom: 30
  },
  foot: {
    paddingTop: 40
  },
  headText: {
    fontFamily: Def.fontExtLight,
    fontSize: 30,
    color: Def.colorWhite
  }
};


const mapStateToProps = ({ env, date, auth }) => {
  return { env, date, auth };
};

export default connect(mapStateToProps, { modalLogoutSet, todoReset, authReset, dateReset })(Logout);
