import React, { Component } from 'react';
import { Animated, Easing, Text, View, Keyboard, LayoutAnimation, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {
  taskFormTitleChanged, taskFormTitleErrorChanged, taskFormDateChanged, taskFormReset, modalTaskFormSet, modalCurtainSet, todoAdd, todoChange, todoMove, todoDelete
} from '../../actions';
import Modaler from './Modaler';
import { ButtonBlue, TextButtonBlue, IconButton, Input, DatePicker } from '../common';
import Icon from '../common/Icons';
import InputDate from '../common/InputDate'; // not from index.js, because desktop
import { Def, Txt, Func } from '../../config';
import { Tracker, Gac } from '../../ga';

class TaskFormMobile extends Component {

  state = {
    height: new Animated.Value(0),
    marginTop: new Animated.Value(this.props.env.height),
    cHeight: new Animated.Value(this.props.env.height),
    cOpacity: new Animated.Value(0),
    headDuration: Def.modalDuration,
    formDuration: Def.modalDuration,
    lastpos: 0,
    calendar: false,
    savepressed: false,
  }

  componentWillMount() {
    if (this.props.todo.datas !== null) {
      this.setLastpos(this.props);
    }

    if (this.props.modal.taskformType === 'add') {
      Tracker.screenView(Gac.scr.addtask);
    } else {
      Tracker.screenView(Gac.scr.edittask);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.task.inputtitle !== nextProps.task.inputtitle) return true;
    if (this.props.task.inputdate !== nextProps.task.inputdate) return true;
    if (this.props.task.inputtitleError !== nextProps.task.inputtitleError) return true;
    if (this.state.calendar !== nextState.calendar) return true;
    if (this.props.todo.settings.autoCorrection !== nextProps.todo.settings.autoCorrection) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.calendar !== nextState.calendar) {
      if (nextState.calendar) {
        Animated.sequence([
          Animated.delay(100),
          Animated.parallel([
            Animated.timing(this.state.cHeight, { toValue: 220, duration: this.state.formDuration * 0.6 }),
            Animated.timing(this.state.cOpacity, { toValue: 1, duration: this.state.formDuration * 0.6 }),
          ])
        ]).start();
      }
    }
  }

  onShow() {
    Animated.sequence([
      Animated.delay(10),
      Animated.parallel([
        Animated.timing(this.state.height, { toValue: 110, easing: Easing.out(Easing.back(0.8)), duration: this.state.headDuration }),
        Animated.timing(this.state.marginTop, { toValue: 0, easing: Easing.out(Easing.back(0.8)), duration: this.state.formDuration }),
      ])
    ]).start(() => {
      this.props.modalCurtainSet(true);
      if (this.props.modal.taskformType === 'add') {
        setTimeout(() => { this.refs.TaskTitleInput.refs.input.focus(); }, 100);
      }
    });
  }

  onCancelPress() {
    Keyboard.dismiss();
    this.onRemove();
    Tracker.userAction(Gac.usract.taskformcancel);
  }

  setLastpos({ todo, date }) {
    if (Func.dayAvailable(todo.datas, date.current)) {
      const tasks = Object.keys(todo.datas[date.current]).map(tsks => { return Object.assign({}, todo.datas[date.current][tsks], { uid: tsks }); });
      const maxpos = ((tasks.length > 0) ? Math.max.apply(Math, tasks.map((o) => { return o.pos; })) : 0);
      this.setState({ lastpos: maxpos });
    }
  }

  startSave(type) {
    Keyboard.dismiss();
    this.onRemove(type);
  }

  onRemove(type = 'cancel') {
    if (!this.state.savepressed) {
      this.setState({ savepressed: true });
      this.props.modalCurtainSet(false);

      Animated.sequence([
        Animated.delay(10),
        Animated.parallel([
          Animated.timing(this.state.height, { toValue: 0, duration: this.state.headDuration * 0.7 }),
          Animated.timing(this.state.marginTop, { toValue: this.props.env.height, duration: this.state.formDuration * 0.7 }),
        ])
      ]).start(() => {
        if (type === 'add') {
          Tracker.userAction(Gac.usract.taskformadd);
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          this.props.todoAdd({ text: this.props.task.inputtitle, done: false, date: this.props.date.current, pos: (this.state.lastpos + 1) });
        } else if (type === 'edit') {
          Tracker.userAction(Gac.usract.taskformedit);
          const { done, date, pos, uid } = this.props.task.taskdatas;
          if (this.props.task.inputdate === date) {
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            this.props.todoChange({ text: this.props.task.inputtitle, done, date, pos, uid });
          } else {
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            this.props.todoMove({
              uid,
              olddate: date,
              newdate: this.props.task.inputdate,
              data: { text: this.props.task.inputtitle, done, pos },
            });
          }
        }
        this.props.taskFormReset();
        this.props.modalTaskFormSet(false);
      });
    }
  }

  onDateChange(date) {
    this.props.taskFormDateChanged(date);
    this.calendarClose();
  }

  onSavePress() {
    const { inputtitle } = this.props.task;
    let formIsValid = true;
    this.props.taskFormTitleErrorChanged('');

    if (inputtitle.length < 2) {
      this.props.taskFormTitleErrorChanged(Txt.taskform.titleerror);
      formIsValid = false;
    }

    if (formIsValid) {
      if (this.props.modal.taskformType === 'add') {
        this.startSave(this.props.modal.taskformType);
      } else {
        this.startSave(this.props.modal.taskformType);
      }
    }
  }

  pressCalendarCancel() {
    this.calendarClose();
    Tracker.userAction(Gac.usract.taskformdateclose);
  }

  calendarOpen() {
    Keyboard.dismiss();
    this.setState({ calendar: true });
    Tracker.userAction(Gac.usract.taskformdate);
  }

  calendarClose() {
    Animated.parallel([
      Animated.timing(this.state.cHeight, { toValue: this.props.env.height, duration: this.state.formDuration * 0.7 }),
      Animated.timing(this.state.cOpacity, { toValue: 0, duration: this.state.formDuration * 0.7 }),
    ]).start(() => {
      this.setState({ calendar: false });
    });
  }

  onSubmitEditing() {
    this.onSavePress();
  }

  renderHeaderStyle() {
    const height = this.state.height;
    return [styles.header, { height }];
  }

  renderTitle() {
    if (this.props.modal.taskformType === 'add') {
      return Txt.taskform.addtitle;
    }

    return Txt.taskform.edittitle;
  }

  renderHeader() {
    return (
      <Animated.View style={this.renderHeaderStyle()}>
        <View style={styles.titlecont}>
          <Text style={styles.title}>{this.renderTitle()}</Text>
        </View>
        <View style={styles.closercont}>
          <IconButton icon="close" color={Def.colorWhite} onPress={this.onCancelPress.bind(this)} />
        </View>

      </Animated.View>
    );
  }

  renderDatePicker() {
    return null;
  }

  onTitleChange(text) {
    this.props.taskFormTitleChanged(text);
  }


  renderFormContent() {
    return (
      <View style={styles.fromspace}>
        <View style={styles.section}>
          <Input
            ref="TaskTitleInput"
            onChangeText={this.onTitleChange.bind(this)}
            value={this.props.task.inputtitle}
            keyboardType="default"
            placeholder={Txt.taskform.tasktitleplaceholder}
            errorText={this.props.task.inputtitleError}
            onSubmitEditing={this.onSubmitEditing.bind(this)}
            autoCorrect={this.props.todo.settings.autoCorrection}
            inverse
          />
        </View>

        <View style={styles.section}>
          <InputDate date={this.props.task.inputdate} onPress={this.calendarOpen.bind(this)} editable={(this.props.modal.taskformType === 'edit')} />
        </View>


        <View style={[styles.section, styles.savecont]}>
          <ButtonBlue onPress={this.onSavePress.bind(this)} inverse>{(this.props.modal.taskformType === 'add') ? Txt.taskform.addbutton : Txt.taskform.editbutton}</ButtonBlue>
        </View>

        <View style={[styles.section, styles.cancelcont]}>
          <TextButtonBlue onPress={this.onCancelPress.bind(this)} inverse>{Txt.taskform.cancel}</TextButtonBlue>
        </View>

      </View>
    );
  }

  renderFormStyle() {
    const marginTop = this.state.marginTop;
    return [styles.form, { marginTop }];
  }

  renderForm() {
    if (this.props.env.wide) {
      return (
        <Animated.View style={this.renderFormStyle()}>
          <View style={styles.formwrapper}>
            {this.renderFormContent()}
          </View>
        </Animated.View>
      );
    }

    return (
      <Animated.View style={this.renderFormStyle()}>
        {this.renderFormContent()}
      </Animated.View>
    );
  }

  renderCalendar() {
    if (!this.state.calendar) return null;

    return (
      <View style={styles.calandarWrapper}>
        <View style={styles.calendarCover}>
          <Animated.View style={[styles.calendarBg, { opacity: this.state.cOpacity }]} />

          <Animated.View style={[styles.calendarCancel, { height: this.state.cHeight }]}>
            <TouchableOpacity onPress={this.pressCalendarCancel.bind(this)} style={styles.calendarTouch}>
              <View style={styles.cancelCalendarIcon}>
                <Icon src="right" fill="#FFFFFF" />
              </View>
            </TouchableOpacity>
          </Animated.View>

          <View style={styles.calendarCont}>
            <View style={styles.calendarScale}>
              <DatePicker
                firstdaySun={this.props.todo.settings.firstdaySun}
                dateSelected={this.onDateChange.bind(this)}
                today={this.props.task.inputdate}
                selectedDate={this.props.task.inputdate}
                animating={false}
                fixed
              />
            </View>
          </View>

        </View>
      </View>
    );
  }

  render() {
    if (this.props.env.isdesktop) return null;

    return (
      <Modaler {...this.props} animationType={'none'} onShow={this.onShow.bind(this)}  onRequestClose={this.onCancelPress.bind(this)} hardwareAccelerated transparent>
        <View style={styles.inmodaler}>
          <View style={styles.headcont}>
            {this.renderHeader()}
          </View>
          <View style={styles.formcont}>
            {this.renderForm()}
          </View>

          {this.renderCalendar()}
        </View>
      </Modaler>
    );
  }
}


const styles = {
  inmodaler: {
    flex: 1, // is important
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
  },

  calandarWrapper: {
    position: 'absolute',
    zIndex: 500,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  calendarCover: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
  },
  calendarBg: {
    position: 'absolute',
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: Def.colorBlueTransparent
  },
  calendarCont: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: Def.colorGray,
  },
  calendarScale: {
    transform: [{ scale: 1.25 }],
    marginTop: 60,
  },
  calendarCancel: {
    height: 220,
  },
  calendarTouch: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cancelCalendarIcon: {
    marginBottom: 5,
    transform: [{ rotate: '90deg' }, { scale: 0.8 }],
  },

  headcont: {
    height: 110,
    overflow: 'hidden',
  },
  formcont: {
    flex: 1,
    overflow: 'hidden',
  },
  header: {
    height: 110, // animated
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 4,
    backgroundColor: Def.colorBlue,
  },
  closercont: {
    paddingBottom: 2,
  },
  titlecont: {
    paddingBottom: 14,
  },
  title: {
    fontFamily: Def.fontLight,
    fontSize: 24,
    color: Def.colorWhite
  },
  form: {
    flex: 1,
    marginTop: 0, // animated
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 30,
    backgroundColor: Def.colorWhite,
  },
  formwrapper: {

    width: 400,
    alignSelf: 'center',
  },
  fromspace: {
  },
  section: {
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingBottom: 0,
  },
  savecont: {
    paddingTop: 60,
    paddingBottom: 30
  },
  pickerIOS: {
    flex: 1, // important
    justifyContent: 'center',  // important
  },
  DatePickerAndroid: {
    fontFamily: Def.fontMedium,
    color: Def.colorBlack,
    fontSize: 18,
    lineHeight: 30,
    flex: 1,
    paddingLeft: 0,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'left',
    textAlignVertical: 'bottom',
    paddingTop: 4,
    paddingBottom: 4,
    height: 36,
    marginTop: 18,
    flexDirection: 'column',
    borderWidthRight: 0,
    borderBottomWidth: 1,
    borderBottomColor: Def.colorBlue,
  }
};


const mapStateToProps = ({ env, date, task, modal, todo }) => {
  return { env, date, task, modal, todo };
};

export default connect(mapStateToProps, {
  taskFormTitleChanged, taskFormTitleErrorChanged, taskFormDateChanged, taskFormReset, modalTaskFormSet, modalCurtainSet, todoAdd, todoChange, todoMove, todoDelete
})(TaskFormMobile);
