import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, Modal, Animated, Easing } from 'react-native';
import { connect } from 'react-redux';
import { modalMenuSet, modalLogoutSet, modalSettingsSet, modalHowtoSet, dateJump } from '../../actions';
import { IconButtonLabel, DatePicker } from '../common';
import { Def, Txt, Func } from '../../config';
import { Tracker, Gac } from '../../ga';

class Menu extends Component {

  state = {
    opacity: new Animated.Value(0),
    marginLeft: new Animated.Value(-300),
    animDuration: Def.modalDuration,
    startday: new Date(),
    selectedday: new Date(),
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.date.today !== nextProps.date.today) return true;
    if (this.props.date.current !== nextProps.date.current) return true;
    if (this.props.date.anim !== nextProps.date.anim) return true;
    if (this.props.visible !== nextProps.visible) return true;
    if (nextProps.todo.settings !== null) {
      if (this.props.todo.settings.firstdaySun !== nextProps.todo.settings.firstdaySun) { return true; }
    }

    return false;
  }

  componentDidUpdate() {
    if (this.props.visible) {
      if (!this.props.env.wide) {
        Tracker.screenView(Gac.scr.menu);
      }
    }
  }

  onShow() {
    Animated.sequence([
      Animated.delay(10),
      Animated.parallel([
        Animated.timing(this.state.marginLeft, { toValue: 0, easing: Easing.out(Easing.back(0.8)), duration: this.state.animDuration }),
        Animated.timing(this.state.opacity, { toValue: 1, duration: this.state.animDuration }),
      ])
    ]).start();
  }

  dateSelected(date) {
    this.moveToDate(date);
    Tracker.userAction(Gac.usract.calshowdate);
  }

  pressYesterday() {
    this.moveToDate(this.props.date.yesterday);
    Tracker.userAction(Gac.usract.yesterday);
  }

  pressToday() {
    this.moveToDate(this.props.date.today);
    Tracker.userAction(Gac.usract.today);
  }

  pressTomorrow() {
    this.moveToDate(this.props.date.tomorrow);
    Tracker.userAction(Gac.usract.tomorrow);
  }

  moveToDate(date) {
    if (this.props.env.wide) {
      if (date !== this.props.date.current) { this.props.dateJump(date); }
    } else {
      Animated.parallel([
        Animated.timing(this.state.marginLeft, { toValue: -this.props.env.width, duration: (this.state.animDuration * 0.6) }),
        Animated.timing(this.state.opacity, { toValue: 0, duration: (this.state.animDuration * 0.6) }),
      ]).start(() => {
        this.props.modalMenuSet(false);
        if (date !== this.props.date.current) { this.props.dateJump(date); }
      });
    }
  }

  pressClose() {
    Tracker.userAction(Gac.usract.menuclose);
    Animated.parallel([
      Animated.timing(this.state.marginLeft, { toValue: -this.props.env.width, duration: (this.state.animDuration * 0.6) }),
      Animated.timing(this.state.opacity, { toValue: 0, duration: (this.state.animDuration * 0.6) }),
    ]).start(() => {
      if (!this.props.env.wide) {
        this.props.modalMenuSet(false);
      }
    });
  }

  pressLogout() {
    Tracker.userAction(Gac.usract.logout);
    if (this.props.env.wide) {
      this.props.modalLogoutSet(true);
    } else {
      Animated.parallel([
        Animated.timing(this.state.marginLeft, { toValue: -this.props.env.width, duration: (this.state.animDuration * 0.6) }),
        Animated.timing(this.state.opacity, { toValue: 0, duration: (this.state.animDuration * 0.6) }),
      ]).start(() => {
        if (!this.props.env.wide) {
          this.props.modalMenuSet(false);
          this.props.modalLogoutSet(true);
        }
      });
    }
  }

  pressSettings() {
    Tracker.userAction(Gac.usract.settings);
    if (this.props.env.wide) {
      this.props.modalSettingsSet(true);
    } else {
      Animated.parallel([
        Animated.timing(this.state.marginLeft, { toValue: -this.props.env.width, duration: (this.state.animDuration * 0.6) }),
        Animated.timing(this.state.opacity, { toValue: 0, duration: (this.state.animDuration * 0.6) }),
      ]).start(() => {
        if (!this.props.env.wide) {
          this.props.modalMenuSet(false);
          this.props.modalSettingsSet(true);
        }
      });
    }
  }

  pressHowto() {
    Tracker.userAction(Gac.usract.howto);
    if (this.props.env.wide) {
      this.props.modalHowtoSet(true);
    } else {
      Animated.parallel([
        Animated.timing(this.state.marginLeft, { toValue: -this.props.env.width, duration: (this.state.animDuration * 0.6) }),
        Animated.timing(this.state.opacity, { toValue: 0, duration: (this.state.animDuration * 0.6) }),
      ]).start(() => {
        if (!this.props.env.wide) {
          this.props.modalMenuSet(false);
          this.props.modalHowtoSet(true);
        }
      });
    }
  }

  pressBackground() {
    Tracker.userAction(Gac.usract.menuclosebg);
    this.pressClose();
  }

  renderCloser() {
    if (this.props.env.wide) return null;

    return (
      <View style={styles.closercont}>
        <IconButtonLabel icon="close" label={Txt.menu.closemenu} onPress={this.pressClose.bind(this)} />
      </View>
    );
  }

  renderMenuStyle() {
    if (!this.props.env.wide) {
      const { marginLeft } = this.state;
      if (this.props.env.isandroid) {
        return [styles.menu, { marginLeft, marginTop: -10 }];
      }
      return [styles.menu, { marginLeft }];
    }
    return styles.menu;
  }

  renderBottomStyle() {
    return this.props.env.wide ? [styles.bottom, styles.bottomwide] : styles.bottom;
  }

  renderContent() {
    return (
      <Animated.View style={this.renderMenuStyle()} className="menuView">
        <View style={styles.top}>
          <IconButtonLabel icon="day" label={Txt.menu.yesterday} onPress={this.pressYesterday.bind(this)}  type={(this.props.date.current === this.props.date.yesterday) ? 'black' : 'blue'} />
          <IconButtonLabel icon="day" label={Txt.menu.today} onPress={this.pressToday.bind(this)} type={(this.props.date.current === this.props.date.today) ? 'black' : 'blue'} />
          <IconButtonLabel icon="day" label={Txt.menu.tomorrow} onPress={this.pressTomorrow.bind(this)} type={(this.props.date.current === this.props.date.tomorrow) ? 'black' : 'blue'} />
        </View>

        <View style={styles.calendarcont} className="MenuCalendar">
          <DatePicker
            firstdaySun={this.props.todo.settings.firstdaySun}
            dateSelected={this.dateSelected.bind(this)}
            today={this.props.date.today}
            selectedDate={this.props.date.current}
            animating={this.props.date.anim}
          />
        </View>

        <View style={this.renderBottomStyle()}>
          <IconButtonLabel icon="settings" label={Txt.menu.settings} onPress={this.pressSettings.bind(this)} />
          <IconButtonLabel icon="bulb" label={Txt.menu.howto} onPress={this.pressHowto.bind(this)} />

          <IconButtonLabel icon="logout" label={Txt.menu.logout} onPress={this.pressLogout.bind(this)} />
        </View>

        {this.renderCloser()}
      </Animated.View>
    );
  }

  renderBgStyle() {
    const { opacity } = this.state;
    const animStyle = { opacity };
    return [styles.bg, animStyle];
  }

  renderMenu() {
    if (this.props.env.wide) {
      return this.renderContent();
    }

    return (
      <Modal visible={this.props.visible} animationType={'none'} onShow={this.onShow.bind(this)} onRequestClose={this.pressClose.bind(this)} hardwareAccelerated transparent>
        <Animated.View style={this.renderBgStyle()} />
        <View style={styles.modalcont}>
          <TouchableWithoutFeedback onPress={this.pressBackground.bind(this)}><View style={styles.bgpress} /></TouchableWithoutFeedback>
          <View style={styles.menucont}>
            {this.renderContent()}
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return this.renderMenu();
  }
}

const styles = {
  bg: {
    position: 'absolute',
    zIndex: 200,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: Def.colorBlueTransparent
  },
  bgpress: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  modalcont: {
    flex: 1,
    zIndex: 500,
  },
  menucont: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    backgroundColor: Def.colorWhite,
  },
  menu: {
    flex: 1,
    alignItems: 'stretch',
    paddingTop: Def.menuPaddingTop-2,
    backgroundColor: Def.colorWhite,
    marginLeft: 0,
  },
  top: {
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'flex-start',
  },
  bottom: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  bottomwide: {
    justifyContent: 'flex-end'
  },
  closercont: {
    height: 50,
    paddingTop: 6,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'flex-start',
  },
  calendarcont: {
    width: 300,
    height: 296,
    paddingTop: 20,
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.05)'
  }
};


const mapStateToProps = ({ env, date, modal, todo }) => {
  return { env, date, modal, todo };
};

export default connect(mapStateToProps, { modalMenuSet, modalLogoutSet, modalSettingsSet, modalHowtoSet, dateJump })(Menu);
