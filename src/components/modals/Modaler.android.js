import React, { Component } from 'react';
import { Modal } from 'react-native';

class Modaler extends Component {
  render() {
    return (
      <Modal {...this.props} transparent>
        {this.props.children}
      </Modal>
    );
  }
}

export default Modaler;
