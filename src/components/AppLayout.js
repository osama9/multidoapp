import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Animated } from 'react-native';
import { Def } from '../config';
import { setDesktopHeader } from '../actions';
import DayList from './daylist/DayList';
import Menu from './modals/menu';
import { Tracker, Gac } from '../ga';

class AppLayout extends Component {

  state = {
    opacity: new Animated.Value(0),
    animDuration: 200,
  }

  componentWillMount() {
    setDesktopHeader(this.props.env.platf, 'icon', 'blue');
  }

  componentDidMount() {
    Tracker.screenView(Gac.scr.applayout);
    const delay = this.props.modal.howtoIsFirst ? Def.modalDuration : 100;
    Animated.sequence([
      Animated.delay(delay),
      Animated.timing(this.state.opacity, { toValue: 1, duration: this.state.animDuration })
    ]).start();
  }

  componentWillUnmount() {
    setDesktopHeader(this.props.env.platf, 'icon', '');
  }

  renderContent() {
    return <DayList showAdd={this.props.showAdd} />;
  }

  renderContentStyle() {
    if (this.props.env.isandroid && this.props.env.wide) {
      return [styles.content, { width: (this.props.env.width - Def.menubarWidth) }];
    }
    return [styles.content, { flex: 1 }];
  }

  renderAppLayout() {
    if (this.props.env.wide) {
      return (
        <Animated.View style={[styles.container, { opacity: this.state.opacity }]}>
          <View style={styles.menubar}>
            <Menu visible />
          </View>
          <View style={this.renderContentStyle()}>
            {this.renderContent()}
          </View>

        </Animated.View>
      );
    }

    return (
      <Animated.View style={[styles.container, { opacity: this.state.opacity }]}>
        <View style={this.renderContentStyle()}>
          {this.renderContent()}
        </View>
        <Menu visible={this.props.modal.menuVisible} />
      </Animated.View>
    );
  }

  render() {
    return this.renderAppLayout();
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  menubar: {
    width: Def.menubarWidth,
    overflow: 'hidden',
    backgroundColor: Def.colorGray,
  },
  content: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: Def.colorBlue,
  }
};

const mapStateToProps = ({ env, modal }) => {
  return { env, modal };
};

export default connect(mapStateToProps)(AppLayout);
