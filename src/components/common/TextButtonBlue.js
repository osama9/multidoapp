import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Def } from '../../config';

const TextButtonBlue = ({ onPress, children }) => {
  return (
    <View style={styles.view} className="buttonCenter">
      <TouchableOpacity onPress={onPress} style={styles.button} >
        <Text style={styles.text}>{children}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  text: {
    fontFamily: Def.fontLight,
    alignSelf: 'center',
    color: Def.colorBlue,
    fontSize: 18,
    paddingTop: 15,
    paddingBottom: 13
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 0,
    borderWidth: 0
  },
  view: {
    flex: 1,
    height: Def.buttonHeight,
  }
};

export { TextButtonBlue };
