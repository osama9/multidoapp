import React, { Component } from 'react';
import { TextInput, TouchableOpacity, View, Animated, Easing, Platform } from 'react-native';
import Icon from './Icons';
import { Def } from '../../config';

class InputPassword extends Component {

  state = {
    opacity: new Animated.Value(0.5),
    top: new Animated.Value(22),
    fontSize: new Animated.Value(18),
    errorOpacity: new Animated.Value(0),
    errorSize: new Animated.Value(0),
    animDuration: 500,
    placeholderBounce: 4,
    errorBounce: 2,
    secure: true,
  };

  componentWillMount() {
    if (this.props.value !== '') {
      this.setActive();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value !== nextProps.value) return true;
    if (this.props.errorText !== nextProps.errorText) return true;
    if (this.state.secure !== nextState.secure) return true;
    if (this.props.type !== nextProps.type) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.value !== '') {
      this.setActive();
    } else {
      this.setDeactive();
    }
  }

  componentDidUpdate() {
    if (this.props.errorText !== '') {
      Animated.parallel([
        Animated.timing(this.state.errorOpacity, { toValue: 1, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
        Animated.timing(this.state.errorSize, { toValue: 12, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
      ]).start();
    } else {
      Animated.parallel([
        Animated.timing(this.state.errorOpacity, { toValue: 0, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
        Animated.timing(this.state.errorSize, { toValue: 0, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
      ]).start();
    }
  }

  onFocus() {
    this.setActive();
  }

  onToggleSecure() {
    this.setState({ secure: !this.state.secure });
  }

  setActive() {
    Animated.parallel([
      Animated.timing(this.state.top, { toValue: 0, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration }),
      Animated.timing(this.state.opacity, { toValue: 1, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration }),
      Animated.timing(this.state.fontSize, { toValue: 14, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration }),
    ]).start();
  }

  setDeactive() {
    Animated.parallel([
      Animated.timing(this.state.top, { toValue: 22, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration * 0.5 }),
      Animated.timing(this.state.opacity, { toValue: 0.5, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration * 0.5 }),
      Animated.timing(this.state.fontSize, { toValue: 18, easing: Easing.out(Easing.back(this.state.placeholderBounce)), duration: this.state.animDuration * 0.5 }),
    ]).start();
  }

  renderPlaceholderStyle() {
    const { opacity, top } = this.state;
    const animStyle = { opacity, top };
    return [styles.placeholderStyle, animStyle];
  }

  renderPlaceholderStyleText() {
    const { fontSize } = this.state;
    if (this.props.inverse) { return [styles.placeholderTextStyle, styles.placeholderTextStyleInverse, { fontSize }]; }
    return [styles.placeholderTextStyle, { fontSize }];
  }

  renderErrorText() {
    const opacity = this.state.errorOpacity;
    const fontSize = this.state.errorSize;
    return [styles.errorStyle, { opacity, fontSize }];
  }

  renderStyle() {
    const error = { borderBottomColor: Def.colorRed };
    if (this.props.errorText !== '') { return [styles.containerStyle, error]; }
    if (this.props.inverse) { return [styles.containerStyle, styles.containerStyleInverse]; }
    return styles.containerStyle;
  }

  renderInputStyle() {
    if (this.props.inverse) { return [styles.inputStyle, styles.inputStyleInverse]; }
    return styles.inputStyle;
  }

  renderToggleStyle() {
    return this.state.secure ? styles.button : [styles.button, styles.buttonset];
  }

  renderProps() {
    return Platform.OS === 'android' ? { underlineColorAndroid: 'transparent' } : null;
  }

  render() {
    return (
      <View style={styles.wrapperStyle}>
        <Animated.View style={this.renderPlaceholderStyle()}>
          <Animated.Text style={this.renderPlaceholderStyleText()}>{this.props.placeholder}</Animated.Text>
        </Animated.View>

        <View style={this.renderStyle()}>
          <TextInput
            ref="input"
            secureTextEntry={this.state.secure}
            keyboardType={this.props.keyboardType}
            autoCorrect={false}
            autoCapitalize={this.props.autoCapitalize}
            style={this.renderInputStyle()}
            value={this.props.value}
            onChangeText={this.props.onChangeText}
            onFocus={this.onFocus.bind(this)}
            className="input"
            onSubmitEditing={this.props.onSubmitEditing}
            autoFocus={this.props.autoFocus}
            autoComplete="off"
            {... this.renderProps()}
          />

          <View style={this.renderToggleStyle()}>
            <TouchableOpacity onPress={this.onToggleSecure.bind(this)} activeOpacity={1} style={styles.touchable}>
              <Icon src="eye" fill={Def.colorWhite} />
            </TouchableOpacity>
          </View>

        </View>
        <Animated.Text style={this.renderErrorText()}>
          {this.props.errorText}
        </Animated.Text>
      </View>
    );
  }
}

const styles = {
  inputStyle: {
    fontFamily: Def.fontMedium,
    color: Def.colorWhite,
    fontSize: 18,
    lineHeight: 30,
    flex: 1,
    paddingLeft: 0,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'left',
    textAlignVertical: 'bottom',
    paddingTop: 4,
    paddingBottom: 4,
  },
  inputStyleInverse: {
    color: Def.colorBlack,
  },
  placeholderStyle: {
    position: 'absolute',
    height: 30,
    left: 0,
    opacity: 0.5,
    top: 27,
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  placeholderTextStyle: {
    alignSelf: 'flex-start',
    fontFamily: Def.fontLight,
    color: Def.colorWhite,
    fontSize: 18,
    lineHeight: 20,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  placeholderTextStyleInverse: {
    color: Def.colorBlue,
  },
  errorStyle: {
    marginTop: 2,
    fontFamily: Def.fontMedium,
    color: Def.colorRed,
    fontSize: 12,
    flex: 1,
    justifyContent: 'center',
    textAlign: 'right',
  },
  containerStyle: {
    height: 36,
    marginTop: 18,
    flexDirection: 'row',
    alignItems: 'stretch',
    borderBottomWidth: 1,
    borderBottomColor: Def.colorWhite,
  },
  containerStyleInverse: {
    borderBottomColor: Def.colorBlue,
  },
  wrapperStyle: {
    height: 72,
    flex: 1,
    position: 'relative',
  },
  touchable: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingBottom: 12,
  },
  button: {
    width: 40,
    height: 40,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    opacity: 0.3,
  },
  buttonset: {
    opacity: 1,
  }
};

export { InputPassword };
