import React from 'react';
import { View, Image } from 'react-native';

const Loader = ({ color = '#FFFFFF' }) => {
  if (color === 'blue') {
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: './assets/images/desktop/loader-gray.gif' }} />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={{ uri: './assets/images/desktop/loader.gif' }} />
    </View>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 24,
    height: 24
  }
};


export { Loader };
