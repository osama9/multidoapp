import React, { Component } from 'react';
import { Platform, View, Text, TouchableOpacity, Animated } from 'react-native';
import { Def, Txt } from '../../config';
import { Button, Input, InputPassword, ButtonLoad, Link } from '../common';

class Registration extends Component {

  renderButton() {
    if (this.props.loading) return <ButtonLoad />;
    return <Button onPress={this.props.pressButton}>{Txt.registration.button}</Button>;
  }

  render() {
    return (
      <Animated.View style={[styles.container, this.props.rotation]} className="auth-container registration">
        <View style={[styles.section, styles.head]}>
          <Text style={styles.headText}>{Txt.registration.title.Break()}</Text>
        </View>
        <View style={styles.section}>
          <Input
            onChangeText={this.props.emailChangeText}
            value={this.props.emailValue}
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder={Txt.registration.email}
            errorText={this.props.emailError}
            onSubmitEditing={this.props.onSubmitEditing}
            type={this.props.type}
          />
        </View>
        <View style={styles.section}>
          <InputPassword
            onChangeText={this.props.passwordChangeText}
            value={this.props.passwordValue}
            placeholder={Txt.registration.password}
            errorText={this.props.passwordError}
            onSubmitEditing={this.props.onSubmitEditing}
            type={this.props.type}
          />
        </View>
        <View style={[styles.section, styles.foot]}>
          {this.renderButton()}
        </View>

        <View style={styles.extra}>
          <TouchableOpacity style={styles.reg} onPress={this.props.pressRight}>
            <Text style={styles.regText}>{Txt.registration.link}</Text>
          </TouchableOpacity>

          <Link url={Txt.howto.url} style={styles.linktext}>{Txt.howto.link}</Link>
        </View>

      </Animated.View>
    );
  }
}

const styles = {
  container: {
    paddingTop: (Platform.OS === 'ios') ? 42 : 32,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    alignSelf: Def.wide ? 'stretch' : 'flex-start',
  },
  section: {
    flexDirection: 'row',
    paddingBottom: 0
  },
  head: {
    paddingBottom: 12
  },
  foot: {
    paddingTop: 16
  },
  headText: {
    fontFamily: Def.fontExtLight,
    fontSize: 30,
    color: Def.colorWhite
  },
  extra: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingTop: 20,
  },
  reg: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  regText: {
    fontFamily: Def.fontMedium,
    color: Def.colorWhite,
    fontSize: 14,
  },
  linktext: {
    fontFamily: Def.fontLight,
    color: Def.colorWhite,
    marginTop: 13,
    fontSize: 14,
  }
};

export default Registration;
