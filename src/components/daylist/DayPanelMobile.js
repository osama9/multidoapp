import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, Animated, UIManager, Platform } from 'react-native';
import SortableListView from 'react-native-sortable-listview';
import { todoChange, todoSort, taskSetSortable, taskSetSum, modalTaskFormTypeSet, modalTaskFormSet, dateJump } from '../../actions';
import { Def, Txt, Func } from '../../config';
import TaskRow from './TaskRowMobile';
import DayHeader from './DayHeader';
import { Tracker, Gac } from '../../ga';


class DayPanelMobile extends Component {

  state = {
    opacity: new Animated.Value(1),
    isAvailable: false,
    emptyOpacity: new Animated.Value(0),
    emptyMargin: new Animated.Value(-100),
    emptyDuration: 500,
    scrollEnabled: false,
    removeClippedSubviews: false,
  };

  componentWillMount() {
    this.createDataSource(this.props);

    const { available } = this.props.date;

    if (available === this.props.day) {
      Animated.timing(this.state.opacity, { toValue: 0, duration: Def.listDuration, }).start(() => {
        this.setState({ isAvailable: true });
        this.emptyAnimIn();
      });
    } else {
      this.setState({ isAvailable: false });
      Animated.timing(this.state.opacity, { toValue: 0.8, duration: Def.listDuration, }).start(() => {
        this.emptyAnimOut();
      });
    }
  }


  shouldComponentUpdate(nextProps, nextState) {
    if (!Func.objEquivalent(this.props.rawtasks, nextProps.rawtasks)) return true;
    if (this.props.windowSize !== nextProps.windowSize) return true;
    if (this.state.scrollHeight !== nextState.scrollHeight) return true;
    if (this.state.scrollEnabled !== nextState.scrollEnabled) return true;
    if (this.state.removeClippedSubviews !== nextState.removeClippedSubviews) return true;
    if (this.props.task.sortablelist !== nextProps.task.sortablelist) return true;
    if (this.state.isAvailable !== nextState.isAvailable) return true;

    if (this.props.date.available !== nextProps.date.available) {
      if (nextProps.date.available === this.props.day) {
        return true;
      } else if (this.props.date.available === this.props.day) {
        return true;
      }
    }

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!Func.objEquivalent(this.props.rawtasks, nextProps.rawtasks)) {
      this.createDataSource(nextProps);
    }

    if (this.props.date.available !== nextProps.date.available) {
      if (nextProps.date.available === this.props.day) {
        Animated.timing(this.state.opacity, { toValue: 0, duration: Def.listDuration, }).start(() => {
          this.setState({ isAvailable: true });
          this.emptyAnimIn();
        });
      } else if (this.props.date.available === this.props.day) {
        this.setState({ isAvailable: false });
        Animated.timing(this.state.opacity, { toValue: 0.8, duration: Def.listDuration, }).start(() => {
          this.emptyAnimOut();
        });
      }
    }
  }

  emptyAnimIn() {
    Animated.parallel([
      Animated.timing(this.state.emptyOpacity, { toValue: 1, duration: this.state.emptyDuration }),
      Animated.timing(this.state.emptyMargin, { toValue: 0, duration: this.state.emptyDuration }),
    ]).start();
  }

  emptyAnimOut() {
    Animated.timing(this.state.emptyOpacity, { toValue: 0, duration: this.state.emptyDuration * 0.6 }).start(() => {
      this.state.emptyMargin.setValue(-100);
    });
  }

  createDataSource({ rawtasks }) {
    let tasks = {}; let obj = {};
    if (typeof rawtasks === 'object' && rawtasks != null) {
      const alltasks = Object.keys(rawtasks).map(tsks => { return Object.assign({}, rawtasks[tsks], { uid: tsks, date: this.props.day }); });
      const doneTasks = alltasks.filter(t => t.done).sort((a, b) => { return b.pos - a.pos; });
      const undoneTasks = alltasks.filter(t => !t.done).sort((a, b) => { return b.pos - a.pos; });
      tasks = undoneTasks; tasks.push(...doneTasks);
      obj = {}; tasks.forEach(data => { obj[data.uid] = data; });
    }

    const height = ((tasks.length) * (Def.taskHeight + Def.taskMarginBottom));
    const scrollHeight = Def.stageHeight - (50 + 80 + Def.paddingTop);
    this.setState({ scrollEnabled: (scrollHeight < height) });

    this.dataSource = obj;
  }


  forceUpdate(newOrder) {
    const newObject = {};
    newOrder.forEach((value, key) => {
      const position = (Object.keys(this.dataSource).length - 1) - key;
      newObject[this.dataSource[value].uid] = { text: this.dataSource[value].text, done: this.dataSource[value].done, pos: position };
    });
    this.props.todoSort(newObject, this.props.day);
    Tracker.userAction(Gac.usract.rowdroptosort);
  }

  onMoveStart() {
    if (Platform.OS === 'android') { UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(false); }
    if (Platform.OS === 'ios') { this.setState({ removeClippedSubviews: true }); }
  }

  onMoveEnd() {
    if (Platform.OS === 'android') { UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true); }
    if (Platform.OS === 'ios') { this.setState({ removeClippedSubviews: false }); }
  }

  emptyAdd() {
    this.props.modalTaskFormTypeSet('add');
    this.props.modalTaskFormSet(true);
  }

  moveToHere() {
    this.props.dateJump(this.props.day);
  }

  emptyTitleStyle() {
    const { isandroid, wide } = this.props.env;
    if (wide) {
      if (isandroid) {
        return [styles.emptyTitle, styles.emptyTitleAndroid, styles.emptyTitleWide];
      } else {
        return [styles.emptyTitle, styles.emptyTitleWide];
      }
    }

    if (isandroid) {
      return [styles.emptyTitle, styles.emptyTitleAndroid]
    }

    return [styles.emptyTitle];
  }

  renderEmpty() {
    const { wide } = this.props.env;
    const opacity = this.state.emptyOpacity;
    const marginBottom = this.state.emptyMargin;
    return (
      <Animated.View style={wide ? [styles.emptyView, styles.emptyViewWide, { opacity, marginBottom }] : [styles.emptyView, { opacity, marginBottom }]}>
        <TouchableOpacity activeOpacity={0.6} onPress={this.emptyAdd.bind(this)}>
          <Text style={this.emptyTitleStyle()}>{Txt.daypanel.listempty.Break()}</Text>
          <Text style={wide ? [styles.emptyText, styles.emptyTextWide] : [styles.emptyText]}>{Txt.daypanel.addhere}</Text>
        </TouchableOpacity>
      </Animated.View>
    );
  }

  renderListView() {
    const order = Object.keys(this.dataSource);
    if (!order.length) { return this.renderEmpty(); }

    return (
      <SortableListView
        ref='SortableListView'
        removeClippedSubviews={this.state.removeClippedSubviews}
        scrollEnabled={this.state.scrollEnabled}
        style={styles.content}
        data={this.dataSource}
        order={order}
        rowHasChanged={(r1, r2) => r1 !== r2}
        sortRowStyle={styles.sortrow}
        onRowMoved={e => {
          order.splice(e.to, 0, order.splice(e.from, 1)[0]);
          this.forceUpdate(order);
        }}
        onMoveStart={this.onMoveStart.bind(this)}
        onMoveEnd={this.onMoveEnd.bind(this)}
        renderRow={row => <TaskRow data={row} />}
      />
    );
  }

  renderCurrent() {
    let width = { width: Def.wideDaysWidth };
    if (!this.props.env.wide) {
      width = { width: this.props.env.width };
    }
    return [styles.day, width];
  }

  renderOverlayStyle() {
    const opacity = this.state.opacity;
    const animStyle = { opacity };
    if (this.state.isAvailable) { return null; }
    return [styles.overlay, animStyle];
  }

  renderDay() {
    return (
      <View style={this.renderCurrent()} ref="DayPanelView">
        <View style={styles.top}>
          <DayHeader date={this.props.day} />
        </View>

        {this.renderListView()}

        <Animated.View style={this.renderOverlayStyle()}><TouchableOpacity onPress={this.moveToHere.bind(this)} style={styles.overlayButton} /></Animated.View>
      </View>
    );
  }

  render() {
    return this.renderDay();
  }
}

const styles = {
  overlay: {
    position: 'absolute',
    opacity: 1,
    left: 10,
    right: 10,
    top: 0,
    bottom: 0,
    backgroundColor: Def.colorBlue,
  },
  overlayButton: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  scrollview: {
    flex: 1,
    flexDirection: 'column',
  },
  customscrollview: {
    flex: 1,
    backgroundColor: 'rgba(0,255,0,0.3)'
  },
  top: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  content: {
    flex: 1,
  },
  current: {
    opacity: 1,
  },
  sortrow: {
    opacity: 0.9,
  },

  emptyView: {
    flex: 1,
    opacity: 0,
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  emptyViewWide: {
    alignItems: 'center',
  },
  emptyTitle: {
    textAlign: 'right',
    fontFamily: Def.fontExtLight,
    color: 'rgba(255,255,255,0.4)',
    fontSize: 48,
    lineHeight: 44,
  },
  emptyTitleAndroid: {
    height: 102,
  },
  emptyTitleWide: {
    textAlign: 'center',
  },
  emptyText: {
    paddingTop: 10,
    opacity: 1,
    textAlign: 'right',
    fontFamily: Def.fontMedium,
    color: Def.colorWhite,
    fontSize: 16,
  },
  emptyTextWide: {
    textAlign: 'center',
  }
};

const mapStateToProps = (state, ownProps) => {
  const rawtasks = state.todo.datas ? state.todo.datas[ownProps.day] : {};
  return { ...state, rawtasks };
};

export default connect(mapStateToProps, { todoChange, todoSort, taskSetSortable, taskSetSum, modalTaskFormTypeSet, modalTaskFormSet, dateJump })(DayPanelMobile);
