import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { modalMenuSet, modalTaskFormSet, modalTaskFormTypeSet, modalCurtainSet, taskDateSet } from '../../actions';
import { IconButton } from '../common';
import { Def } from '../../config';
import { Tracker, Gac } from '../../ga';

class FooterMobile extends Component {

  state = {
    today: true
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.date.current !== nextProps.date.current) return true;
    if (this.props.date.today !== nextProps.date.today) return true;
    if (this.state.today !== nextState.today) return true;

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.date.current !== nextProps.date.current) {
      this.props.taskDateSet(nextProps.date.current);
      this.setState({ today: (nextProps.date.current === nextProps.date.today) });
    }

    if (this.props.date.today !== nextProps.date.today) {
      this.setState({ today: (nextProps.date.current === nextProps.date.today) });
    }
  }

  pressAdd() {
    this.props.modalTaskFormTypeSet('add');
    this.props.modalTaskFormSet(true);
    Tracker.userAction(Gac.usract.footplus);
  }

  pressMenu() {
    this.props.modalMenuSet(true);
    Tracker.userAction(Gac.usract.footmenu);
  }

  render() {
    if (this.props.env.wide) {
      return (
        <View style={styles.view}>
          <View style={styles.icontwide}>
            <IconButton icon="left" style={styles.icon} onPress={this.props.pressPrev} ontouch />
          </View>
          <View style={styles.icontwide}>
            <IconButton icon="plus" onPress={this.pressAdd.bind(this)} reverse ontouch />
          </View>
          <View style={styles.icontwide}>
            <IconButton icon="right" style={styles.icon} onPress={this.props.pressNext} ontouch />
          </View>
        </View>
      );
    }
    return (
      <View style={styles.view}>
        <View style={styles.left}>
          <IconButton icon="menu" onPress={this.pressMenu.bind(this)} ontouch />
        </View>
        <View style={styles.center}>
          <View style={styles.icont}>
            <IconButton icon="left" style={styles.icon} onPress={this.props.pressPrev} ontouch />
          </View>
          <View style={styles.icont}>
            <IconButton icon="ring" style={styles.icon} onPress={this.props.pressToday} today={this.state.today} ontouch />
          </View>
          <View style={styles.icont}>
            <IconButton icon="right" style={styles.icon} onPress={this.props.pressNext} ontouch />
          </View>
        </View>
        <View style={styles.right}>
          <IconButton icon="plus" onPress={this.pressAdd.bind(this)} reverse ontouch />
        </View>
      </View>
    );
  }
}

const styles = {
  view: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    backgroundColor: Def.colorWhite
  },
  left: {
    flex: 1,
    alignItems: 'flex-start',
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',

  },
  center: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icont: {
    width: 80,
    alignItems: 'center',
  },
  icontwide: {
    flex: 1,
    alignItems: 'center',
  }
};


const mapStateToProps = ({ env, date }) => {
  return { env, date };
};

export default connect(mapStateToProps, { modalMenuSet, modalTaskFormSet, modalTaskFormTypeSet, modalCurtainSet, taskDateSet })(FooterMobile);
