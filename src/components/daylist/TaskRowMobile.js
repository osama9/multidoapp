import React, { Component } from 'react';
import { View, Text, TouchableOpacity, PanResponder, Animated, LayoutAnimation, Platform } from 'react-native';
import { connect } from 'react-redux';
import { taskSetSortable, todoChange, todoDelete, modalTaskFormTypeSet, modalTaskFormSet, taskFormSet } from '../../actions';
import Icon from '../common/Icons';
import { Def } from '../../config';
import { Tracker, Gac } from '../../ga';

class TaskRowMobile extends Component {

  state = {
    lastPress: 0,
    sortWidth: new Animated.Value(0),
    delWidth: new Animated.Value(0),
    pipeWidth: new Animated.Value(Def.taskHeight),
    panDuration: 1,
    animDuration: 200,
    animBounce: 4,
    mode: 'normal', // normal, sort, delete
    animated: (Platform.OS === 'ios'),
    conf: {
      sens: 20,
      pback: 0.9,
      limit: (Platform.OS === 'ios') ? 60 : Def.taskHeight,
      accepted: 30,
      width: Def.taskHeight,
    }
  };

  componentWillMount() {
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderMove: this.handlePanResponderMove.bind(this),
      onPanResponderRelease: this.handlePanResponderRelease.bind(this),
      onPanResponderEnd: this.handlePanResponderEnd.bind(this),
      onPanResponderTerminate: this.handlePanResponderTerminate.bind(this),
      onShouldBlockNativeResponder: _ => false,
    });

    if (!this.props.sortHandlers) {
      this.state.sortWidth.setValue(this.state.conf.width);
      this.state.pipeWidth.setValue(0);
      this.setState({ mode: 'sort' });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.task.sortablelist !== nextProps.task.sortablelist) return true;
    if (this.state.mode !== nextState.mode) return true;
    if (this.props.data.text !== nextProps.data.text) return true;
    if (this.props.data.done !== nextProps.data.done) return true;

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!nextProps.data.done) {
      if (nextState.mode !== 'delete') {
        if (nextProps.task.sortablelist) {
          this.setState({ mode: 'sort' });
        } else {
          this.setState({ mode: 'normal' });
        }
      }
    } else {
      this.setState({ mode: 'normal' });
    }
  }

  componentDidUpdate() {
    if (this.state.mode === 'sort' && !this.props.data.done) {
      if (this.state.animated) {
        Animated.parallel([
          Animated.timing(this.state.sortWidth, { toValue: Def.taskHeight, duration: this.state.animDuration }),
          Animated.timing(this.state.pipeWidth, { toValue: Def.taskHeight, duration: this.state.animDuration }),
          Animated.timing(this.state.delWidth, { toValue: 0, duration: this.state.animDuration }),
        ]).start();
      } else {
        this.state.sortWidth.setValue(Def.taskHeight);
        this.state.pipeWidth.setValue(Def.taskHeight);
        this.state.delWidth.setValue(0);
      }
    } else {
      if (this.state.animated) {
        Animated.timing(this.state.sortWidth, { toValue: 0, duration: this.state.animDuration }).start();
      } else {
        this.state.sortWidth.setValue(0);
      }
    }
  }

  handlePanResponderMove(e: Object, gestureState: Object) {
    const { dx } = gestureState;
    const { sens, pback, limit } = this.state.conf;
    const dxabs = Math.abs(dx);

    if (dx > 0) {
      if (dx > sens) {
        let size; let psize;
        switch (this.state.mode) {
          case 'normal':
            size = (dxabs * pback) - sens; size = (size > limit) ? limit : size; size = (size > 0) ? size : 0;
            psize = ((Def.taskHeight - size) > 0) ? (Def.taskHeight - size) : 0;
            if (this.state.animated) {
              Animated.parallel([
                Animated.timing(this.state.pipeWidth, { toValue: psize, duration: this.state.panDuration }),
                Animated.timing(this.state.delWidth, { toValue: size, duration: this.state.panDuration }),
              ]).start();
            } else {
              this.state.delWidth.setValue(size);
              this.state.pipeWidth.setValue(psize);
            }
            break;
          case 'sort':
            size = (dxabs * pback) - sens; size = (size > limit) ? limit : size; size = (size > 0) ? size : 0;
            psize = ((Def.taskHeight - size) > 0) ? (Def.taskHeight - size) : 0;
            if (this.state.animated) {
              Animated.timing(this.state.sortWidth, { toValue: psize, duration: this.state.panDuration }).start();
            } else {
              this.state.sortWidth.setValue(psize);
            }
            break;
          default: break;
        }
      }
    } else {
      if (dx < -sens) {
        let size;
        switch (this.state.mode) {
          case 'normal':
            size = (dxabs * pback) - sens; size = (size > limit) ? limit : size; size = (size > 0) ? size : 0;
            if (this.state.animated) {
              Animated.timing(this.state.sortWidth, { toValue: size, duration: this.state.panDuration }).start();
            } else {
              this.state.sortWidth.setValue(size);
            }
            break;
          case 'delete':
            size = (dxabs * pback) - sens; size = (size > limit) ? limit : size; size = (size > 0) ? size : 0;
            const psize = ((Def.taskHeight - size) > 0) ? (Def.taskHeight - size) : 0;
            if (this.state.animated) {
              Animated.parallel([
                Animated.timing(this.state.pipeWidth, { toValue: size, duration: this.state.panDuration }),
                Animated.timing(this.state.delWidth, { toValue: psize, duration: this.state.panDuration }),
              ]).start();
            } else {
              this.state.pipeWidth.setValue(size);
              this.state.delWidth.setValue(psize);
            }
            break;
          default: break;
        }
      }
    }
  }

  handlePanResponderEnd(e: Object, gestureState: Object) {
    this.handlePanResponderReleaseGesture(e, gestureState);
  }

  handlePanResponderTerminate(e: Object, gestureState: Object) {
    this.handlePanResponderReleaseGesture(e, gestureState);
  }

  handlePanResponderRelease(e: Object, gestureState: Object) {
    /// DOUBLE PRESS
    const delta = new Date().getTime() - this.state.lastPress;
    if (delta < 400) { this.doublePress(); }
    this.setState({ lastPress: new Date().getTime() });

    this.handlePanResponderReleaseGesture(e, gestureState);
  }

  handlePanResponderReleaseGesture(e: Object, gestureState: Object) {
    const { dx } = gestureState;
    const { sens, pback, accepted, width } = this.state.conf;
    const dxabs = Math.abs(dx);

    if (dx > 0) {
      if (dx > sens) {
        let size;
        switch (this.state.mode) {
          case 'normal':
            size = (dxabs * pback) - sens; size = (size > accepted) ? width : 0;
            if (size > accepted) {
              this.state.pipeWidth.setValue(0);
              if (this.state.animated) {
                Animated.timing(this.state.delWidth, { toValue: size, duration: this.state.animDuration }).start(() => { this.setState({ mode: 'delete' }); });
                this.deletableSet();
              } else {
                this.state.delWidth.setValue(size);
                this.setState({ mode: 'delete' });
                this.deletableSet();
              }
            } else {
              if (this.state.animated) {
                Animated.parallel([
                  Animated.timing(this.state.delWidth, { toValue: 0, duration: this.state.animDuration }),
                  Animated.timing(this.state.pipeWidth, { toValue: width, duration: this.state.animDuration }),
                ]).start();
              } else {
                this.state.delWidth.setValue(0);
                this.state.pipeWidth.setValue(width);
              }
            }
            break;
          case 'sort':
            size = (dxabs * pback) - sens; size = (size > accepted) ? width : 0;
            if (size > accepted) {
              if (this.state.animated) {
                Animated.timing(this.state.sortWidth, { toValue: 0, duration: this.state.animDuration }).start(() => {
                  this.sortableUnset();
                });
              } else {
                this.state.sortWidth.setValue(0);
                this.sortableUnset();
              }
            } else {
              if (this.state.animated) {
                Animated.timing(this.state.sortWidth, { toValue: width, duration: this.state.animDuration }).start();
              } else {
                this.state.sortWidth.setValue(width);
              }
            }
            break;
          default: break;
        }
      }
    } else {
      if (dx < -sens) {
        let size;
        switch (this.state.mode) {
          case 'normal':
            size = (dxabs * pback) - sens; size = (size > accepted) ? width : 0;
            if (size > accepted) {
              if (this.state.animated) {
                Animated.timing(this.state.sortWidth, { toValue: size, duration: this.state.animDuration }).start(() => {
                  this.sortableSet();
                });
              } else {
                this.state.sortWidth.setValue(size);
                this.sortableSet();
              }
            } else {
              if (this.state.animated) {
                Animated.timing(this.state.sortWidth, { toValue: 0, duration: this.state.animDuration }).start();
              } else {
                this.state.sortWidth.setValue(0);
              }
            }
            break;
          case 'delete':
            size = (dxabs * pback) - sens; size = (size > accepted) ? width : 0;
            if (size > accepted) {
              this.state.delWidth.setValue(0);
              if (this.state.animated) {
                Animated.timing(this.state.pipeWidth, { toValue: size, duration: this.state.animDuration }).start(() => { this.setState({ mode: 'normal' }); });
                this.deletableUnset();
              } else {
                this.state.pipeWidth.setValue(size);
                this.setState({ mode: 'normal' });
                this.deletableUnset();
              }
            } else {
              if (this.state.animated) {
                Animated.parallel([
                  Animated.timing(this.state.pipeWidth, { toValue: 0, duration: this.state.animDuration }),
                  Animated.timing(this.state.delWidth, { toValue: width, duration: this.state.animDuration }),
                ]).start();
              } else {
                this.state.pipeWidth.setValue(0);
                this.state.delWidth.setValue(width);
              }
            }
            break;
          default: break;
        }
      }
    }
  }

  sortableSet() {
    if (!this.props.task.sortablelist) {
      this.setState({ mode: 'sort' });
      this.props.taskSetSortable(true);
      Tracker.userAction(Gac.usract.rowswipesetsort);
    }
  }

  sortableUnset() {
    if (this.props.task.sortablelist) {
      this.setState({ mode: 'normal' });
      this.props.taskSetSortable(false);
      Tracker.userAction(Gac.usract.rowswipeunsetsort);
    }
  }

  deletableSet() {
    Tracker.userAction(Gac.usract.rowswipesetdelete);
  }

  deletableUnset() {
    Tracker.userAction(Gac.usract.rowswipeunsetdelete);
  }

  doublePress() {
    this.props.taskFormSet({ title: this.props.data.text, date: this.props.data.date, taskdatas: this.props.data });
    this.props.modalTaskFormTypeSet('edit');
    this.props.modalTaskFormSet(true);
    Tracker.userAction(Gac.usract.rowdoublepress);
  }

  taskDelete() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.todoDelete({ uid: this.props.data.uid, date: this.props.data.date });
    Tracker.userAction(Gac.usract.rowremove);
  }

  taskDone() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const { date, done, pos, text, uid } = this.props.data;
    this.props.todoChange({ text, done: !done, date, pos, uid });
    Tracker.userAction(Gac.usract.rowdone);
  }

  sortStyle() {
    if (this.props.data.done) return [styles.sortCont, styles.sortContDone, { width: this.state.sortWidth }];
    return [styles.sortCont, { width: this.state.sortWidth }];
  }

  sortButtonStyle() {
    if (this.props.data.done) return [styles.sortButton, styles.sortButtonDone];
    return styles.sortButton;
  }

  deleteStyle() {
    if (this.state.mode === 'delete') return [styles.deleteRight, { width: Def.taskHeight }];
    return styles.deleteRight;
  }

  panStyle() {
    if (this.props.data.done) return styles.pannerHidden;
    if (this.state.mode === 'delete') return styles.pannerDelete;
    return styles.panner;
  }

  renderViewStyle() {
    if (this.props.data.done) return [styles.view, styles.viewdone];
    return styles.view;
  }

  renderRow() {
    return (
      <View style={styles.container} key={`item-${this.props.data.uid}`}>

        <View style={styles.row}>

          <Animated.View style={[styles.delCont, { width: this.state.delWidth }]}>
            <TouchableOpacity activeOpacity={0.6} style={styles.delButton} onPress={this.taskDelete.bind(this)}>
              <Icon src="bin" fill={Def.colorWhite} />
            </TouchableOpacity>
          </Animated.View>

          <View style={styles.textCont}>
            <Text style={styles.textData} lineBreakMode="tail" numberOfLines={1}>{this.props.data.text}</Text>
          </View>

          <Animated.View style={[styles.pipeCont, { width: this.state.pipeWidth }]}>
            <TouchableOpacity activeOpacity={0.6} style={styles.pipeButton} onPress={this.taskDone.bind(this)}>
              <Icon src="pipe" fill={this.props.data.done ? Def.colorWhite : Def.colorIcon} />
            </TouchableOpacity>
          </Animated.View>

        </View>

        <View style={this.panStyle()} {...this.panResponder.panHandlers} />

        <Animated.View style={this.sortStyle()}>
          <TouchableOpacity activeOpacity={0.6} style={this.sortButtonStyle()} delayLongPress={10} {...this.props.sortHandlers}>
            <Icon src="sort" fill="rgba(0,0,0,0.15)" />
          </TouchableOpacity>
        </Animated.View>

      </View>

    );
  }

  render() {
    return (
      <View style={this.renderViewStyle()}>
        {this.renderRow()}
      </View>
    );
  }
}

const styles = {
  view: {
    height: Def.taskHeight,
    backgroundColor: Def.colorWhite,
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
    paddingLeft: 0,
    paddingRight: 0,
    marginBottom: Def.taskMarginBottom,
    opacity: 1,
  },
  viewdone: {
    backgroundColor: 'rgba(255,255,255,0.2)',
    shadowOpacity: 0,
  },

  panner: {
    position: 'absolute',
    zIndex: 300,
    top: 0,
    bottom: 0,
    left: 0,
    right: Def.taskHeight,
  },
  pannerDelete: {
    position: 'absolute',
    zIndex: 300,
    top: 0,
    bottom: 0,
    left: Def.taskHeight,
    right: 0,
  },

  pannerHidden: {
    position: 'absolute',
    zIndex: 0,
    top: -5,
    left: -5,
  },

  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },

  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },

  sortCont: {
    position: 'absolute',
    zIndex: 300,
    top: 0,
    bottom: 0,
    right: 0,
    opacity: 1,
    width: Def.taskHeight,
    overflow: 'hidden',
    backgroundColor: Def.colorWhite
  },
  sortContDone: {
    backgroundColor: Def.colorBlue
  },
  sortButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.05)'
  },
  sortButtonDone: {
    backgroundColor: 'rgba(255,255,255,0.4)'
  },
  delCont: {
    opacity: 1,
    width: 0, //44,
    overflow: 'hidden',
    backgroundColor: Def.colorRed
  },
  delButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textCont: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textData: {
    paddingTop: Def.taskTextPaddingTop,
    paddingLeft: Def.taskPaddingLeft,
    paddingRight: Def.taskPaddingRight,
    fontFamily: Def.fontMedium,
    color: Def.colorBlack,
    fontSize: Def.taskTextSize,
  },

  pipeCont: {
    opacity: 1,
    width: Def.taskHeight,
    overflow: 'hidden',
  },
  pipeButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },


};

const mapStateToProps = ({ task }) => {
  return { task };
};

export default connect(mapStateToProps, { taskSetSortable, todoChange, todoDelete, modalTaskFormTypeSet, modalTaskFormSet, taskFormSet })(TaskRowMobile);
