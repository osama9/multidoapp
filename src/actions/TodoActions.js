import firebase from '../firebase';
import { ACTYPS } from './types';

export const todoFetch = () => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    dispatch({ type: ACTYPS.TODO.FETCH, payload: true });
    const ref = firebase.database().ref(`/users/${currentUser.uid}`);
    ref.on('value', snapshot => {
      dispatch({ type: ACTYPS.TODO.FETCHSUCCESS, payload: snapshot.val() });
    });
  };
};

export const todoReset = () => {
  return (dispatch) => {
    firebase.auth().signOut().then(() => {
      dispatch({ type: ACTYPS.TODO.RESET });
    });
  };
};

export const todoSetinit = () => {
  return {
    type: ACTYPS.TODO.SETINIT
  };
};

export const todoAdd = ({ text, done, date, pos }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/todos/${date}`)
    .push({ text, done, pos })
    .then(() => {
      dispatch({ type: ACTYPS.TODO.ADD });
    });
  };
};

export const todoChange = ({ text, done, date, pos, uid }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/todos/${date}/${uid}`)
    .set({ text, done, pos })
    .then(() => {
      dispatch({ type: ACTYPS.TODO.CHANGE });
    });
  };
};

export const todoDelete = ({ uid, date }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/todos/${date}/${uid}`)
    .remove()
    .then(() => {
      dispatch({ type: ACTYPS.TODO.DELETE });
    });
  };
};

export const todoMove = ({ uid, olddate, newdate, data, callback = null }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/todos/${olddate}/${uid}`)
    .remove()
    .then(() => {
      firebase.database().ref(`/users/${currentUser.uid}/todos/${newdate}`)
      .push(data)
      .then(() => {
        dispatch({ type: ACTYPS.TODO.MOVE });
      });
    });
  };
};

export const todoSort = (sortObject, date, callback = null) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/todos/${date}/`)
    .update(sortObject)
    .then(() => {
      dispatch({ type: ACTYPS.TODO.SORT });
      if (callback !== null) callback();
    });
  };
};

export const todoSettings = ({ firstdaySun, autoCorrection }) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/settings`)
    .set({ firstdaySun, autoCorrection })
    .then(() => {
      dispatch({ type: ACTYPS.TODO.SETTINGS });
    });
  };
};

export const todoVisited = (platform) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/visited/${platform}`)
    .set(true)
    .then(() => {
      dispatch({ type: ACTYPS.TODO.VISITED });
    });
  };
};
