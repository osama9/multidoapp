import firebase from 'firebase';
import { Conf } from './config';

const FbApp = firebase.initializeApp(Conf.firebase);

export default FbApp;
