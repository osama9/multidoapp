import $ from 'jquery';
import { Txt } from './';

const Areas = {
  menuwidth: 300,

  current() {
    const panel = $('[data-current="true"]');
    const obj = {
      t: $(panel[0]).offset().top,
      b: $(panel[0]).offset().top + $(panel[0]).height(),
      l: this.leftcrop($(panel[0]).offset().left),
      r: this.leftcrop($(panel[0]).offset().left) + this.widthcrop($(panel[0]).offset().left, $(panel[0]).width()),
    };
    return obj;
  },

  areas() {
    const panels = $('[data-dropdate]');
    const areas = [];
    for (const p of panels) {
      if (($(p).attr('data-type') === 'calendar') || this.visible($(p).offset().left + $(p).width())) {
        const obj = {
          t: $(p).offset().top,
          b: $(p).offset().top + $(p).height(),
          l: ($(p).attr('data-type') === 'list') ? this.leftcrop($(p).offset().left) : $(p).offset().left,
          r: ($(p).attr('data-type') === 'list') ? this.leftcrop($(p).offset().left) + this.widthcrop($(p).offset().left, $(p).width()) : $(p).offset().left + $(p).width(),
          date: $(p).attr('data-dropdate'),
          type: $(p).attr('data-type'),
        };
        areas.push(obj);
      }
    }
    return areas;
  },

  dragfrom(x, y, area) {
    let dragout = true;
    if (x > area.l && x < area.r && y > area.t && y < area.b) { dragout = false; }
    this.setdrag(x, y, dragout);
    return dragout;
  },
  dropto(x, y, areas) {
    let date = null;
    for (const area of areas) { if (x > area.l && x < area.r && y > area.t && y < area.b) { date = area.date; } }
    this.setdate(date);
    return date;
  },
  setdate(date) {
    let d = '?';
    if (date !== null) {
      const ds = date.split('-'); ds[1] = parseInt(ds[1]);
      d = `${Txt.calendar.monthnamesshort[ds[1] - 1]}. ${ds[2]}.`;
    }
    $('#dropHelper').find('span').text(d);
  },
  setdrag(x, y, out) {
    if (out) {
      $('.taskItem').not('#AppContainer .taskItem').addClass('ghostItem');
      $('[data-current="true"]').addClass('noSortableList');
      $('.ghostItem').css('opacity', '0');
      $('.ghostItem').addClass('hidden');
      $('#dropHelper').css('top', `${y + 4}px`);
      $('#dropHelper').css('left', `${x - 37}px`);
      $('#dropHelper').css('opacity', '1');
      $('#dropHelper').addClass('show');
    } else {
      $('.noSortableList').removeClass('noSortableList');
      $('.ghostItem').css('opacity', '1');
      $('.ghostItem').removeClass('hidden');
      $('#dropHelper').css('opacity', '0');
      $('#dropHelper').css('top', '-500px');
      $('#dropHelper').css('left', '-500px');
      $('#dropHelper').removeClass('show');
    }
  },
  remove() {
    $('.noSortableList').removeClass('noSortableList');
    $('#dropHelper').css('opacity', '0');
    $('#dropHelper').css('top', '-500px');
    $('#dropHelper').css('left', '-500px');
    $('#dropHelper').removeClass('show');
  },
  fix() {
    $('.taskItem').attr('style', 'padding-bottom: 10px;');
  },
  visible(right) { return !(right < this.menuwidth); },
  widthcrop(left, width) { return (left < this.menuwidth) ? width - (this.menuwidth - left) : width; },
  leftcrop(left) { return (left < this.menuwidth) ? this.menuwidth : left; },
};

export { Areas };
