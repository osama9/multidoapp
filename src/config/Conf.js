const Conf = {
  firebase: {
      apiKey: '',
      authDomain: '',
      databaseURL: '',
      storageBucket: '',
      messagingSenderId: ''
  },
  //gaid: '', // Google Analytics ID
};

export { Conf };
